﻿using UnityEngine;
using System.Collections;

public enum StateRot{
	Wait = 0,
	Down = 1,
	Up = 2,
	ForceUp = 3,
	End = 4
};

public class PlayerDown : MonoBehaviour {

    [SerializeField]
    private Transform targetTransform;
	
    [SerializeField]
    private float valRotation = -90;
	
	private StateRot rotation = StateRot.Wait;
	
	//Time before force up
    [SerializeField]
	private float timeBeforeUp = 0.2f;
	private float deltaTimeBeforeUp = 0;
	
	
	
	void Update () {
		deltaTimeBeforeUp += Time.deltaTime;
	}
	
    void FixedUpdate()
    {
		if(rotation == StateRot.Down){
			targetTransform.Rotate(new Vector3(valRotation, 0f, 0f));
			deltaTimeBeforeUp = 0;
			rotation = StateRot.Up;
		}
		else if((rotation == StateRot.Up && deltaTimeBeforeUp >= timeBeforeUp) || rotation == StateRot.ForceUp){
			targetTransform.Rotate(new Vector3(-valRotation, 0f, 0f));
			rotation = StateRot.End;
		}
		
    }
	
	public void SetState(StateRot val){
		rotation = val;
	}
	
	
	public StateRot GetState(){
		return rotation;
	}
}
