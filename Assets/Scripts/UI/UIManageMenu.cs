﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManageMenu : MonoBehaviour {

	[SerializeField]
	private CanvasGroup menuCanvasGroup;
	
	[SerializeField]
	private CanvasGroup optionCanvasGroup;
	
	[SerializeField]
	private CanvasGroup confirmQuitCanvasGroup;
	
	[SerializeField]
	private CanvasGroup confirmResetDataCanvasGroup;
	
	[SerializeField]
	private Text textHighScore;
	
	
    private void Awake(){
		
		SetUiCanvasGroup(menuCanvasGroup, 1);
			
		SetUiCanvasGroup(optionCanvasGroup, 0, false);
		SetUiCanvasGroup(confirmQuitCanvasGroup, 0, false);
		SetUiCanvasGroup(confirmResetDataCanvasGroup, 0, false);
		
	}
	
	void Start(){
		textHighScore.text = "HighScore : " + ManageLoad.GetHighScore();
	}
	
	
	public void DoGoGame(){
		SceneManager.LoadScene(ManageInfo.GetNameScene());
	}
	
	
	public void DoQuit(){
		SetUiCanvasGroup(menuCanvasGroup, 0.5f, false);
		
		SetUiCanvasGroup(confirmQuitCanvasGroup, 1);
	}
	
	public void DoConfirmQuitNo(){
		SetUiCanvasGroup(menuCanvasGroup, 1);
		
		SetUiCanvasGroup(confirmQuitCanvasGroup, 0, false);
	}
	
	public void DoConfirmQuitYes(){
		Application.Quit();
	}
	
	
	public void DoOption(){
		SetUiCanvasGroup(menuCanvasGroup, 0.5f, false);
		
		SetUiCanvasGroup(optionCanvasGroup, 1);
	}
	
	public void DoOptionReturnMenu(){
		SetUiCanvasGroup(menuCanvasGroup, 1);
		
		SetUiCanvasGroup(optionCanvasGroup, 0, false);
	}
	
	public void DoResetData(){
		SetUiCanvasGroup(menuCanvasGroup, 0, false);
		SetUiCanvasGroup(optionCanvasGroup, 0.5f, false);
		
		SetUiCanvasGroup(confirmResetDataCanvasGroup, 1);
	}
	
	public void DoConfirmResetDataNo(){
		SetUiCanvasGroup(menuCanvasGroup, 0.5f, false);
		SetUiCanvasGroup(optionCanvasGroup, 1);
		
		SetUiCanvasGroup(confirmResetDataCanvasGroup, 0, false);
	}
	
	public void DoConfirmResetDataYes(){
		SetUiCanvasGroup(menuCanvasGroup, 0.5f, false);
		SetUiCanvasGroup(optionCanvasGroup, 1);
		
		SetUiCanvasGroup(confirmResetDataCanvasGroup, 0, false);
		ManageLoad.ResetFile();
		textHighScore.text = "HighScore : " + ManageLoad.GetHighScore();
	}
	
	private void SetUiCanvasGroup(CanvasGroup canvas, float alpha, bool other = true){
		canvas.alpha = alpha;
		canvas.interactable = other;
		canvas.blocksRaycasts = other;
	}
}
