﻿using UnityEngine;
using System.Collections;

public class ManageArrayPiece : MonoBehaviour {
	
	
	[SerializeField]
	private GameObject[] arrayPiece;
	
	//Reactivate all piece of part
	void OnEnable(){
		for(int i = 0 ; i < arrayPiece.Length ; i++){
			arrayPiece[i].SetActive(true);
		}
	}
}
