﻿using UnityEngine;
using System.Collections;

public class ManageScore : MonoBehaviour {

	[SerializeField]
	private float timeScoreAddMin = 0.1f;
	private static float deltaTimeScoreAdd = 0;
	//Basic multiplier
	private static int multiplier = 1;
	//Multiplier of current zone
	private static int multiplierExtern = 1;
	
	
	void Update () {
		if(!UIManageGame.InWait() && !UIManageGame.HaveLoose()){
			deltaTimeScoreAdd += Time.deltaTime;
				
			if(deltaTimeScoreAdd >= timeScoreAddMin){
				timeScoreAddMin = 0;
				
				ManageMultiplier();
			}
		}
	}
	
	private static void ManageMultiplier(){
		ManageInfo.AddScore(multiplier * multiplierExtern);
		if(ManageInfo.GetScoreCurrent() > 1000 && multiplier == 1){
			multiplier = 2;
		}
		else if(ManageInfo.GetScoreCurrent() > 5000 && multiplier == 2){
			multiplier = 3;
		}
		else if(ManageInfo.GetScoreCurrent() > 12000 && multiplier == 3){
			multiplier = 4;
		}
	}
	
	//Function for TP
	public static void DoPassXSec(float sec){
		deltaTimeScoreAdd = sec;
		
		while(deltaTimeScoreAdd > 0){
			deltaTimeScoreAdd -= Time.deltaTime;
			
			ManageMultiplier();
		}
		deltaTimeScoreAdd = 0;
	}
	
	public static void ChangeExternMultiplier(int val){
		if(val > 0)
			multiplierExtern = val;
	}
}
