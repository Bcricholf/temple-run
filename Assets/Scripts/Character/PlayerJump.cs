﻿using UnityEngine;
using System.Collections;

public class PlayerJump : MonoBehaviour {

    [SerializeField]
    private Rigidbody targetRigidBody;
	
	[SerializeField]
    private Animator animator;

    [SerializeField]
    private float jumpSpeed = 25;
	
	//Number of jump possible
    [SerializeField]
    private float jumpCount = 1;
	
	//Normal gravity
    [SerializeField]
    private float gravity = -15;
	//Gravity when player is on state down
    [SerializeField]
    private float gravityDown = -25;
	private float gravitySave;
	
    private int inJump = 0;
    private int cptGroundCollisions = 0;
    private bool jumpInFixedUpdate = false;
	
	//Time minimum between two jump
    [SerializeField]
	private float deltaTimeMin = 0.5f;
	private float deltaTime = 0;
	
	
	
	void Start () {
		gravitySave = gravity;
	}
	
	void Update () {
	
        if (cptGroundCollisions > 0 && inJump != 0){
						
			if(deltaTime > deltaTimeMin)
				inJump = 0;
		}

		deltaTime += Time.deltaTime;
	}
	
	
    void FixedUpdate()
    {
		if (jumpInFixedUpdate)
		{
			//Add velocity of jump
			targetRigidBody.velocity = new Vector3(
					targetRigidBody.velocity.x,
					jumpSpeed,
					targetRigidBody.velocity.z);
			jumpInFixedUpdate = false;
			inJump++;
		}
		
		//Constant gravity of player
		targetRigidBody.velocity += new Vector3(
			0f,
			gravity * 5f * Time.deltaTime,
			0f);
		
    }
	
    public void OnTriggerEnter(Collider col)
    {
		//Verification if player is on ground
		if(col.tag == "Ground"){
			cptGroundCollisions++;
			
			if(gravity == gravityDown){
				ResetGravity();
			}
			
			animator.SetBool("Idle", true);
		}
    }

    public void OnTriggerExit(Collider col)
    {
        if(col.tag == "Ground"){
			cptGroundCollisions--;
		}
    }
	
	public bool BtnJumpDown()
    {
		if (inJump < jumpCount && deltaTime > deltaTimeMin)
		{
			jumpInFixedUpdate = true;
			
			deltaTime = 0;
			return true;
		}
		return false;
    }
	
	public void SetGravity(int val){
		gravity = val;
	}
	
	public void ResetGravity(){
		gravity = gravitySave;
	}
	
	public void SetGravityDown(){
		gravity = gravityDown;
	}
	
	
	public int GetInJump(){
		return inJump;
	}
	
	public void ResetGroundCollision(){
		cptGroundCollisions = 0;
	}
}
