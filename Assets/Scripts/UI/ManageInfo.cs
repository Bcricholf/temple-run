﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ManageInfo : MonoBehaviour {
	
	
	public static ManageInfo instanceRef;
	
	public static bool isInStage;
	
	public static string nameMenu;
	public static string nameScene;
	
	[SerializeField]
	private string nameMenuNoStatic;
	
	[SerializeField]
	private string nameSceneNoStatic;
	
	private static int scoreCurrent;
	private static int nbPieceCurrent;
	
	private static bool dontSaveData = false;

	
	void Start () {
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}
	
	//Script with just one instance
	void Awake(){
		if(instanceRef == null){
			
			instanceRef = this;
			DontDestroyOnLoad(gameObject);
			
			isInStage = false;
			
			nameMenu = nameMenuNoStatic;
			nameScene = nameSceneNoStatic;
        }
        else
        {
			DestroyImmediate(gameObject);
		}
	}
	
	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode){
        
		if(scene.name != nameMenu){
			isInStage = true;
			
			scoreCurrent = 0;
			nbPieceCurrent = ManageLoad.GetNbPiece();
			dontSaveData = false;
		}
		else{
			if(!dontSaveData)
				SaveData();
		}
	}
		
	public static void SaveData(){
		if(scoreCurrent > ManageLoad.GetHighScore())
			ManageLoad.SetHighScore(scoreCurrent, false);
		
		ManageLoad.SetNbPiece(nbPieceCurrent, false);
		
		ManageLoad.SaveFile();
	}
	
	public static void DontSaveData(){
		dontSaveData = true;
	}
	
	
	public static void SetScoreCurrent(int val){
		scoreCurrent = val;
	}
	
	public static void SetNbPieceCurrent(int val){
		nbPieceCurrent = val;
	}
	
	public static void AddScore(int val){
		scoreCurrent += val;
	}
	
	public static void AddPiece(int val){
		nbPieceCurrent += val;
	}
	
	
	
	
	public static bool GetIsInStage(){
		return isInStage;
	}
	
	public static string GetNameMenu(){
		return nameMenu;
	}
	
	public static string GetNameScene(){
		return nameScene;
	}
	
	public static int GetScoreCurrent(){
		return scoreCurrent;
	}
	
	public static int GetNbPieceCurrent(){
		return nbPieceCurrent;
	}
}
