﻿using UnityEngine;
using System.Collections;

public class PlayerReset : MonoBehaviour {

    [SerializeField]
    private Rigidbody targetRigidBody;
	
	[SerializeField]
	private UIManageGame canvasGame;
	
	[SerializeField]
    private int deathLine = -10;
	
	private bool isDead = false;
	
	
	
    void FixedUpdate()
    {
		//If player fall of road
        if (targetRigidBody.transform.localPosition.y < deathLine && !isDead)
        {
			Mort();
        }
    }
	
	
	void OnCollisionEnter(Collision collision)
    {
		if(!isDead){
			//If player enter collision with obstacle
			if(collision.collider.tag == "Obstacle"){
				Mort();
			}
		}
    }
	
	private void Mort(){
		isDead = true;
		Time.timeScale = 0;
		canvasGame.DoLoose();
	}
}
