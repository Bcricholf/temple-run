﻿using UnityEngine;
using System.Collections;

public class ManagePiece : MonoBehaviour {
	
	[SerializeField]
	private int value = 1;


	void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Player"){
			ManageInfo.AddPiece(value);
			ManageInfo.AddScore(100 * value);
			
			gameObject.SetActive(false);
		}
	}
}
