﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;

public class ManageLoad : MonoBehaviour {

	private static XmlDocument doc = new XmlDocument();
		
	[SerializeField]
	private static string filePath = "/gameData.txt";
		
	private static string pathAuto;
		
	
	void Start(){
		pathAuto = Application.persistentDataPath;

		if (File.Exists(pathAuto + filePath)){
			doc.Load (pathAuto + filePath);
		}
		else {
			//If file no exist, this script create the file
			CreateFileNotFound(pathAuto + filePath);
			doc.Load (pathAuto + filePath);
		}
	}
	
	//For the creation of the file if no exist
	public static void CreateFileNotFound(string path){
		System.IO.File.WriteAllText(path, "");
		
		var sr = File.CreateText(path);
		sr.WriteLine ("<?xml version=\"1.0\"?>");
		sr.WriteLine ("<jeu highScore=\"0\" nbPiece=\"0\" >");
		
        sr.WriteLine ("</jeu>");
		sr.Close();
	}
	
	public static void ResetFile(){
		CreateFileNotFound(pathAuto + filePath);
		doc.Load(pathAuto + filePath);
	}

	public static void SaveFile()
    {
		doc.Save(pathAuto + filePath);
    }
	
	
	
	
	//**************************************************
	//**************************************************
	//                      GET
	//**************************************************
	//**************************************************
	
	
    public static int GetHighScore()
    {
        XmlNode node = doc.SelectSingleNode("/jeu");
        return (int.Parse(node.Attributes["highScore"].InnerText));
    }
	
    public static int GetNbPiece()
    {
        XmlNode node = doc.SelectSingleNode("/jeu");
        return (int.Parse(node.Attributes["nbPiece"].InnerText));
    }
	
	
	
	
	//**************************************************
	//**************************************************
	//                      SET
	//**************************************************
	//**************************************************
	
	
	public static void SetHighScore(int val, bool save = true)
    {
        XmlNode node = doc.SelectSingleNode("/jeu");
        node.Attributes["highScore"].Value = "" + val;
		
		if(save)
			SaveFile();
    }
	
	public static void SetNbPiece(int val, bool save = true)
    {
        XmlNode node = doc.SelectSingleNode("/jeu");
        node.Attributes["nbPiece"].Value = "" + val;
		
		if(save)
			SaveFile();
    }
}
