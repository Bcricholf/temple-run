﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManageGame : MonoBehaviour {

	[SerializeField]
	private CanvasGroup gameCanvasGroup;
	
	[SerializeField]
	private CanvasGroup optionCanvasGroup;
	
	[SerializeField]
	private CanvasGroup confirmQuitCanvasGroup;
	
	[SerializeField]
	private CanvasGroup looseCanvasGroup;
	
	
	[SerializeField]
	private Text textScoreCurrent;
	
	[SerializeField]
	private Text textNbPieceCurrent;
	
	[SerializeField]
	private Text textScoreFinal;
	
	//Boolean for ui of loose
	private static bool loose = false;
	
	//Boolean for ui of option
	private static bool inWait = false;
	
	
	private void Awake(){
		
		SetUiCanvasGroup(gameCanvasGroup, 1);
			
		SetUiCanvasGroup(optionCanvasGroup, 0, false);
		SetUiCanvasGroup(confirmQuitCanvasGroup, 0, false);
		SetUiCanvasGroup(looseCanvasGroup, 0, false);
		
		inWait = false;
		loose = false;
		
	}
	
	void Update(){
		if(!inWait && !loose){
			textScoreCurrent.text = "Score : " + ManageInfo.GetScoreCurrent();
			textNbPieceCurrent.text = "Or : " + ManageInfo.GetNbPieceCurrent();
		}
	}
	
	
	public static bool InWait(){
		return inWait;
	}
	
	public static bool HaveLoose(){
		return loose;
	}
	
	private IEnumerator WaitReturnGame(float waitTime){
		yield return new WaitForSeconds(waitTime);
		inWait = false;
	}
	
	
	public void DoOption(){ 
		inWait = true;
		SetUiCanvasGroup(confirmQuitCanvasGroup, 0, false);
		SetUiCanvasGroup(gameCanvasGroup, 0.5f, false);
		
		SetUiCanvasGroup(optionCanvasGroup, 1);
		Time.timeScale = 0;
	}
	
	public void DoReturnGame(){
		SetUiCanvasGroup(gameCanvasGroup, 1);
		
		SetUiCanvasGroup(optionCanvasGroup, 0, false);
		Time.timeScale = 1;
		
		//Coroutine for no take the tap on option button
		StartCoroutine(WaitReturnGame(0.4f));
	}
	
	
	public void DoLoose(){
		loose = true;
		inWait = true;
		textScoreFinal.text = "Score : " + ManageInfo.GetScoreCurrent();
		
		SetUiCanvasGroup(gameCanvasGroup, 0.5f, false);
		
		SetUiCanvasGroup(looseCanvasGroup, 1);
	}
	
	public void DoLooseRestart(){
		ManageInfo.SaveData();
		
		SetUiCanvasGroup(gameCanvasGroup, 1);
		
		SetUiCanvasGroup(looseCanvasGroup, 0, false);
		
		Time.timeScale = 1;
		
		SceneManager.LoadScene(ManageInfo.GetNameScene());
		inWait = false;
		loose = false;
	}
	
	
	public void DoLooseQuit(){
		ReturnMenu();
	}
	
	
	
	public void DoConfirmQuitNo(){
		SetUiCanvasGroup(gameCanvasGroup, 0.5f, false);
		SetUiCanvasGroup(optionCanvasGroup, 1);
		
		SetUiCanvasGroup(confirmQuitCanvasGroup, 0, false);
	}
	
	public void DoConfirmQuitYes(){
		ManageInfo.DontSaveData();
		ReturnMenu();
	}
	
	public void DoQuit(){
		SetUiCanvasGroup(gameCanvasGroup, 0.3f, false);
		SetUiCanvasGroup(optionCanvasGroup, 0, false);
		
		SetUiCanvasGroup(confirmQuitCanvasGroup, 1);
	}
	
	private void ReturnMenu(){
		Time.timeScale = 1;
		SceneManager.LoadScene(ManageInfo.GetNameMenu());
	}
	
	private void SetUiCanvasGroup(CanvasGroup canvas, float alpha, bool other = true){
		canvas.alpha = alpha;
		canvas.interactable = other;
		canvas.blocksRaycasts = other;
	}
}
