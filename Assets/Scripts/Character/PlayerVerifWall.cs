﻿using UnityEngine;
using System.Collections;

//Script for verification of collision with wall
public class PlayerVerifWall : MonoBehaviour {

	private int countColliding = 0;
	
	void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Wall"){
			countColliding++;
		}
	}

	void OnTriggerExit(Collider collider)
	{
		if(collider.tag == "Wall"){
			countColliding--;
			if(countColliding < 0)
				countColliding = 0;
		}
	}
	
	public bool GetCollision(){
		return (countColliding >= 1);
	}
	
	public void ResetCollision(){
		countColliding = 0;
	}
}
