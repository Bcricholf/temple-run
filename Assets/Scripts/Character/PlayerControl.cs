﻿using UnityEngine;
using System.Collections;


public enum OffSet{
	Left = -1,
	Right = 1
};

public enum Wall{
	Left = 0,
	Right = 1
};

public class PlayerControl : MonoBehaviour {

    [SerializeField]
    private Transform targetTransform;
	
	[SerializeField]
    private PlayerDown targetPlayerDown;
	
	[SerializeField]
    private PlayerJump targetPlayerJump;
	
	[SerializeField]
    private Animator animator;
	
	//Speed of constant move
    [SerializeField]
    private float moveSpeed = 12;
	
	//Part for increase regulary the speed of constant move
    [SerializeField]
    private float valIncreaseSpeed = 0.1f;
    [SerializeField]
	private float timeBeIncreaseSpeed = 5f;
	private float deltaTimeBeIncreaseSpeed = 0;
	
	
	
    private Vector3 movement;
	private int rotation = 0;
	
	//Time minimum between two rotation
    [SerializeField]
	private float timeBeRotateMin = 0.2f;
	private float deltaTimeBeRotate = 0;
	
	//Script verification if rotation is possible
	[SerializeField]
	private PlayerVerifWall[] verifWall;
	
	//Script verification if lag is possible
	[SerializeField]
	private PlayerVerifWall[] verifWallNear;
	
	
	[SerializeField]
	private ManageMap manageMap;
	
	
	private Vector3 firstTouchPos;   
    private Vector3 lastTouchPos; 
	//minimum distance for a swipe to be registered
    private float dragDistance;  
	//midle of the screen for tap
    private float midleX;  
	
	void Start () {
		dragDistance = Screen.height * 15 / 100;
		midleX = Screen.width / 2;
		deltaTimeBeIncreaseSpeed = 0;
	}
	
	
	void Update () {
		//for smartphone
		CheckSwipesInput();
		
		//for pc
		CheckKeysInput();
		
		if(targetPlayerDown.GetState() == StateRot.End){
			targetPlayerDown.SetState(StateRot.Wait);
		}
		
		//Increase of speed constant move
		if(deltaTimeBeIncreaseSpeed > timeBeIncreaseSpeed){
			moveSpeed += valIncreaseSpeed;
			deltaTimeBeIncreaseSpeed = 0;
		}
		
		movement = new Vector3(
				0f,
				0f,
				moveSpeed);
			
			
		deltaTimeBeRotate += Time.deltaTime;
		deltaTimeBeIncreaseSpeed += Time.deltaTime;
		
		if(transform.localPosition.x != 0 || transform.localPosition.z != 0){
			transform.localPosition = new Vector3(0f, transform.localPosition.y, 0f);
		}
	}
	
    void FixedUpdate()
    {
		//Constant move
		targetTransform.Translate(movement * Time.deltaTime);
		
		//Rotation of player
		if(rotation != 0){
			if(deltaTimeBeRotate > timeBeRotateMin){
				targetTransform.Rotate(new Vector3(0f, 90 * rotation, 0f), Space.World);
				ResetDataRotation();
			}
			rotation = 0;
		}
    }
	
	//Function for keyboard input
	private void CheckKeysInput(){
		
		if(!UIManageGame.InWait()){
			if(Input.GetKeyDown("left") && !verifWall[(int)Wall.Left].GetCollision()){
				rotation = -1;
			}
			else if(Input.GetKeyDown("right") && !verifWall[(int)Wall.Right].GetCollision()){
				rotation = 1;
			}
			
			if (Input.GetKeyDown("space"))
			{
				if(targetPlayerDown.GetState() != StateRot.Wait){
					targetPlayerDown.SetState(StateRot.ForceUp);
				}
				if(targetPlayerJump.BtnJumpDown()){
					animator.SetBool("Idle", false);
					animator.SetTrigger("Jump");
				}
			}
			else if(Input.GetKeyDown("down") && targetPlayerDown.GetState() == StateRot.Wait){
				
				targetPlayerJump.SetGravityDown();
				
				targetPlayerDown.SetState(StateRot.Down);
				animator.SetBool("Idle", false);
				animator.SetTrigger("Down");
			}
			
			//OffSet possible if no wall near of player and if the player isn't on a turn
			if (Input.GetKeyDown("a")){
				if(!verifWallNear[(int)Wall.Left].GetCollision() && verifWall[(int)Wall.Left].GetCollision()){
					OffSetHorizontal((int)OffSet.Left);
				}
			}
			if (Input.GetKeyDown("z")){
				if(!verifWallNear[(int)Wall.Right].GetCollision() && verifWall[(int)Wall.Right].GetCollision()){
					OffSetHorizontal((int)OffSet.Right);
				}
			}
		}
	}
	
	//Function for smartphone input
	private void CheckSwipesInput () 
	{
		if(!UIManageGame.InWait()){
			// user is touching the screen with a single touch
			if (Input.touchCount == 1) 
			{
				Touch touch = Input.GetTouch(0); 
				//check for the first touch
				if (touch.phase == TouchPhase.Began) 
				{
					firstTouchPos = touch.position;
					lastTouchPos = touch.position;
				}
				// update the last position based on where they moved
				else if (touch.phase == TouchPhase.Moved) 
				{
					lastTouchPos = touch.position;
				}
				//check if the finger is removed from the screen
				else if (touch.phase == TouchPhase.Ended) 
				{
					lastTouchPos = touch.position;  
	 
					//Check if drag distance is greater than minimum distance to be registered
					if (Mathf.Abs(lastTouchPos.x - firstTouchPos.x) > dragDistance || Mathf.Abs(lastTouchPos.y - firstTouchPos.y) > dragDistance)
					{
						//check if the drag is vertical or horizontal
						if (Mathf.Abs(lastTouchPos.x - firstTouchPos.x) > Mathf.Abs(lastTouchPos.y - firstTouchPos.y))
						{   
							if ((lastTouchPos.x > firstTouchPos.x))  
							{   
								//Right swipe
								if(!verifWall[(int)Wall.Right].GetCollision())
									rotation = 1;
							}
							else
							{   
								//Left swipe
								if(!verifWall[(int)Wall.Left].GetCollision())
									rotation = -1;
							}
						}
						else
						{   
							if (lastTouchPos.y > firstTouchPos.y)  
							{   
								//Up swipe							
								if(targetPlayerDown.GetState() != StateRot.Wait)
									targetPlayerDown.SetState(StateRot.ForceUp);
								
								if(targetPlayerJump.BtnJumpDown()){
									animator.SetBool("Idle", false);
									animator.SetTrigger("Jump");
								}
							}
							else
							{   
								//Down swipe
								if(targetPlayerDown.GetState() == StateRot.Wait){
									targetPlayerJump.SetGravityDown();
									targetPlayerDown.SetState(StateRot.Down);
									animator.SetBool("Idle", false);
									animator.SetTrigger("Down");
								}
							}
						}
					}
					else
					{   
						//If it's a tap
						if(firstTouchPos.x < midleX){
							if(!verifWallNear[(int)Wall.Left].GetCollision() && verifWall[(int)Wall.Left].GetCollision())
								OffSetHorizontal((int)OffSet.Left);
						}
						else{
							if(!verifWallNear[(int)Wall.Right].GetCollision() && verifWall[(int)Wall.Right].GetCollision())
								OffSetHorizontal((int)OffSet.Right);
						}
					}
				}
			}
		}
	}
	
	//As the map is reload, it's necessary to reload collision with the world
	public void ResetDataRotation(bool resetLine = false){
		deltaTimeBeRotate = 0;
		//If resetLine == true, then it's a TP
		if(!resetLine)
			manageMap.ResetMap(GetRotationActuY());
		else
			manageMap.ResetMap(GetRotationActuY(), resetLine, targetTransform.localPosition);
		verifWall[(int)Wall.Left].ResetCollision();
		verifWall[(int)Wall.Right].ResetCollision();
		verifWallNear[(int)Wall.Left].ResetCollision();
		verifWallNear[(int)Wall.Right].ResetCollision();
		targetPlayerJump.ResetGroundCollision();
	}
	
	private void OffSetHorizontal(int direction){
		int rotationCurrentY = GetRotationActuY();
		
		if(rotationCurrentY == 0)
			targetTransform.position = new Vector3(targetTransform.position.x + (0.5f * direction), targetTransform.position.y, targetTransform.position.z);
		else if(rotationCurrentY == 90)
			targetTransform.position = new Vector3(targetTransform.position.x, targetTransform.position.y, targetTransform.position.z + (-0.5f * direction));
		else if(rotationCurrentY == 180)
			targetTransform.position = new Vector3(targetTransform.position.x + (-0.5f * direction), targetTransform.position.y, targetTransform.position.z);
		else if(rotationCurrentY == 270)
			targetTransform.position = new Vector3(targetTransform.position.x, targetTransform.position.y, targetTransform.position.z + (0.5f * direction));
	}
	
	public int GetRotationActuY(){
		return (int)targetTransform.localRotation.eulerAngles.y;
	}
}
