﻿using UnityEngine;
using System.Collections;

public class PowerTP : MonoBehaviour {
	
	[SerializeField]
	private PlayerControl player;

	[SerializeField]
	private float secondPass = 5f;
	
	void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Player"){
			player.ResetDataRotation(true);
			ManageScore.DoPassXSec(secondPass);
			
			gameObject.SetActive(false);
		}
	}
}
