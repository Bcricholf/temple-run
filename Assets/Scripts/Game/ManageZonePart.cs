﻿using UnityEngine;
using System.Collections;

public class ManageZonePart : MonoBehaviour {
	
	
    [SerializeField]
	private ManagePart[] arrayManagePart;
	
    [SerializeField]
	private int multiplierExtern = 1;
	
	public void CreationObject(int idPart, Vector3 movePos, int rotationY){
		if(GoodPart(idPart))
			arrayManagePart[idPart].CreationObject(movePos, rotationY);
	}
		
	public void DeleteAll(){
		for(int i = 0 ; i < arrayManagePart.Length ; i++)
			arrayManagePart[i].DeleteAll();
	}
	
	public Vector3 GetLastPos(int idPart){
		if(GoodPart(idPart))
			return arrayManagePart[idPart].GetLastPos();
		
		return new Vector3(0, 0, 0);
	}
		
	private bool GoodPart(int idPart){
		return (idPart >= 0 && idPart < arrayManagePart.Length);
	}
	
	public int Length(){
		return arrayManagePart.Length;
	}
	
	public int GetMultiplierExtern(){
		return multiplierExtern;
	}
}
