﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TypeZone{
	Basic = 0,
	Other = 1,
	Last = 2
};

public enum TypePart{
	TurnRight = 0,
	TurnLeft = 1,
	TurnDouble = 2,
	Line = 3,
	LineHole = 4,
	LineDown = 5,
	LineJump = 6,
	LineJumpDown = 7,
	LineTP = 8
};

public enum TypePartPiece{
	Nothing = -1,
	Line = 0,
	Jump = 1,
	LineSup = 2,
	Big = 3
};

public class ManageMap : MonoBehaviour {
	
	private List<TypePart> arrayPart = new List<TypePart>();
	private List<TypePartPiece> arrayPartPiece = new List<TypePartPiece>();
	
	private Vector3 posBegin = new Vector3(0, 0, 0);
	private Vector3 posPiece = new Vector3(0, 0, 0);
	
    [SerializeField]
	private ManageZonePart[] arrayManagePartGround;
	private TypeZone zoneCurrent = TypeZone.Basic;
	
	[SerializeField]
	private float timeChangeZone = 30f;
	private static float deltaTimeChangeZone = 0;
	
    [SerializeField]
	private ManagePart[] arrayManagePartPiece;
	
    [SerializeField]
	private int sizePart = 24;
	
	
    [SerializeField]
	private GameObject[] swamp;
	
	private int oldRotation = 0;

	// Use this for initialization
	void Start () {
		//Creation of the first line
		for(int i = 0 ; i < 3 ; i++){
			arrayPart.Add(TypePart.Line);
		}
		arrayPart.Add((TypePart)Random.Range((int)TypePart.TurnRight, (int)TypePart.TurnDouble + 1));
		
		
		for(int i = 0 ; i < arrayPart.Count ; i++){
			if((int)arrayPart[i] < arrayManagePartGround[(int)zoneCurrent].Length()){
				arrayManagePartGround[(int)zoneCurrent].CreationObject((int)arrayPart[i], posBegin, oldRotation);
						
				if(i != arrayPart.Count - 1)
					posBegin = new Vector3(posBegin.x, posBegin.y, posBegin.z + sizePart);
			}
		}
		
		ActivateGoodSwamp();
		
		deltaTimeChangeZone = 0;
	}
	
	void Update(){
		if(!UIManageGame.InWait() && !UIManageGame.HaveLoose()){
			deltaTimeChangeZone += Time.deltaTime;
		}
	}
	
	public void ResetMap(int rotationCurrentY, bool resetLine = false, Vector3 posDeb = new Vector3()){
		
		ResetDataMap(rotationCurrentY, resetLine, posDeb);
		
		CalculGroundMap();
		
		CalculPieceMap();
		
		CreationMap(rotationCurrentY);
	}
	
	private void ResetDataMap(int rotationCurrentY, bool resetLine, Vector3 posDeb){
		TypePart lastPart = arrayPart[arrayPart.Count - 1];
		
		//Reset all data, all part become invisible
		arrayPart.Clear();
		arrayPartPiece.Clear();
		
		for(int i = 0 ; i < arrayManagePartGround.Length ; i++)
			arrayManagePartGround[i].DeleteAll();
		
		arrayPartPiece.Clear();
		for(int i = 0 ; i < arrayManagePartPiece.Length ; i++)
			arrayManagePartPiece[i].DeleteAll();
		
		
		//If use TP
		if(resetLine){
			posBegin = new Vector3(posDeb.x, posBegin.y, posDeb.z);
			
			arrayManagePartGround[(int)zoneCurrent].CreationObject((int)TypePart.Line, posBegin, oldRotation);
		}
		//Else get the last turn
		else
			arrayManagePartGround[(int)zoneCurrent].CreationObject((int)lastPart, posBegin, oldRotation);
		
		
		oldRotation = rotationCurrentY;
		
		TypeZone zoneBeChange = zoneCurrent;
		//Timer for change zone
		if(deltaTimeChangeZone >= timeChangeZone){
			deltaTimeChangeZone = 0;
			TypeZone newZone;
			do{
				newZone = (TypeZone)Random.Range((int)TypeZone.Basic, (int)TypeZone.Last);
			}while(newZone == zoneCurrent);
			zoneCurrent = newZone;
			
			ActivateGoodSwamp();
		}
		
		
		//Change position and rotation of swamp
		if(resetLine)
			swamp[(int)zoneCurrent].transform.localPosition = posBegin;
		else
			swamp[(int)zoneCurrent].transform.position = arrayManagePartGround[(int)zoneBeChange].GetLastPos((int)lastPart);
		swamp[(int)zoneCurrent].transform.localRotation = Quaternion.Euler(0, oldRotation, 0);
		
	}
	
	private void CalculGroundMap(){
		FindLinePart();
		int rand;
		int cpt = 0;
		while(cpt < 3 && arrayPart[arrayPart.Count - 1] > TypePart.TurnDouble){
			rand = Random.Range(0, 4);
			if(rand == 0)
				arrayPart.Add((TypePart)Random.Range((int)TypePart.TurnRight, (int)TypePart.TurnDouble + 1));
			else
				FindLinePart();
			
			cpt++;
		}
		
		if(arrayPart[arrayPart.Count - 1] > TypePart.TurnDouble)
			arrayPart.Add((TypePart)Random.Range((int)TypePart.TurnRight, (int)TypePart.TurnDouble + 1));
	}
	
	//Calcul next part with random
	private void FindLinePart(){
		int rand = Random.Range(0, 2);
		if(rand == 0){
			arrayPart.Add(TypePart.Line);
		}else{
			rand = Random.Range(0, 8);
			
			if(rand == 0)
				arrayPart.Add(TypePart.LineTP);
			else{
				rand = Random.Range(0, 5);
			
				if(rand == 0)
					arrayPart.Add(TypePart.LineJumpDown);
				else
					arrayPart.Add((TypePart)Random.Range((int)TypePart.LineHole, (int)TypePart.LineJump + 1));
			}
		}
	}
	
	private void CalculPieceMap(){
		int rand = 4;
		for(int i = 0 ; i < arrayPart.Count ; i++){
			if(arrayPart[i] > TypePart.TurnDouble){
				//Increase possibility to have piece if the old part have piece
				if(rand == 0)
					rand = Random.Range(0, 3);
				else
					rand = Random.Range(0, 4);
				
				if(rand == 0){
					int rand2 = Random.Range(0, 3);
					if(rand2 == 0){
						rand2 = Random.Range(0, 4);
						if(rand2 == 0)
							arrayPartPiece.Add(TypePartPiece.Big);
						else 
							arrayPartPiece.Add(TypePartPiece.LineSup);
					}
					else
						arrayPartPiece.Add((TypePartPiece)Random.Range((int)TypePartPiece.Line, (int)TypePartPiece.Jump + 1));
				}
				else
					arrayPartPiece.Add(TypePartPiece.Nothing);
			}
			else
				arrayPartPiece.Add(TypePartPiece.Nothing);
		}
	}
	
	private void CreationMap(int rotationCurrentY){
		for(int i = 0 ; i < arrayPart.Count ; i++){
			if((int)arrayPart[i] < arrayManagePartGround[(int)zoneCurrent].Length()){
				//Creation of ground
				if(rotationCurrentY == 0)
					posBegin = new Vector3(posBegin.x, posBegin.y, posBegin.z + sizePart);
				else if(rotationCurrentY == 90)
					posBegin = new Vector3(posBegin.x + sizePart, posBegin.y, posBegin.z);
				else if(rotationCurrentY == 180)
					posBegin = new Vector3(posBegin.x, posBegin.y, posBegin.z - sizePart);
				else if(rotationCurrentY == 270)
					posBegin = new Vector3(posBegin.x - sizePart, posBegin.y, posBegin.z);
				
				arrayManagePartGround[(int)zoneCurrent].CreationObject((int)arrayPart[i], posBegin, oldRotation);
						
						
				
				//Creation of piece
				if((int)arrayPartPiece[i] < arrayManagePartPiece.Length && arrayPartPiece[i] != TypePartPiece.Nothing){
					float dec = 2.0f;
					if(arrayPartPiece[i] == TypePartPiece.Big)
						dec = 0;
					
					if(rotationCurrentY == 0)
						posPiece = new Vector3(posBegin.x + Random.Range(-dec, dec), posBegin.y, posBegin.z);
					else if(rotationCurrentY == 90)
						posPiece = new Vector3(posBegin.x, posBegin.y, posBegin.z + Random.Range(-dec, dec));
					else if(rotationCurrentY == 180)
						posPiece = new Vector3(posBegin.x - Random.Range(-dec, dec), posBegin.y, posBegin.z);
					else if(rotationCurrentY == 270)
						posPiece = new Vector3(posBegin.x, posBegin.y, posBegin.z - Random.Range(-dec, dec));
					
					arrayManagePartPiece[(int)arrayPartPiece[i]].CreationObject(posPiece, oldRotation);
				}
			}
		}
	}
	
	private void ActivateGoodSwamp(){
		for(int i = 0 ; i < swamp.Length ; i++){
			swamp[i].SetActive(false);
		}
		swamp[(int)zoneCurrent].SetActive(true);
	}
}
