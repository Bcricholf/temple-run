﻿using UnityEngine;
using System.Collections;

public class ManagePart : MonoBehaviour {

    [SerializeField]
	private GameObject[] arrayPart;
	private int nextGameObject = 0;
	
	//Activate the next part which are desactivate
	public void CreationObject(Vector3 movePos, int rotationY){
		
		int idBegin = nextGameObject;
		bool end = false;
		
		while(!end){
			if(arrayPart[nextGameObject].activeSelf == false){
				
				arrayPart[nextGameObject].SetActive(true);
				arrayPart[nextGameObject].transform.position = transform.position + movePos;
				arrayPart[nextGameObject].transform.localRotation = Quaternion.Euler(0, rotationY, 0);
				
				end = true;
			}
			if(!end)
				nextGameObject++;
			
			if(nextGameObject >= arrayPart.Length){
				nextGameObject = 0;
			}
			
			if(nextGameObject == idBegin){
				
				end = true;
			}
		}
	}
	
	public Vector3 GetLastPos(){
		return arrayPart[nextGameObject].transform.position;
	}
	
	public void DeleteAll(){
		for(int i = 0 ; i < arrayPart.Length ; i++)
			arrayPart[i].SetActive(false);
	}
}
